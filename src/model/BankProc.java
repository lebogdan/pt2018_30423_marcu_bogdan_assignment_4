package model;

import java.util.HashSet;
import java.util.List;

public interface BankProc {

    void addPerson(Person person);

    void addSavingAccount(String owner, SavingAccount account);

    void withdrawSpendingMoney(String owner, int number, int ammmount);

    void addSpendingAccount(String owner, SpendingAccount account);


    void addSpendingMoney(String owner, int number, int ammmount);

    String[][] getPersons();

    //void addPerson(Person person, HashSet<Account> accounts);

    void deletePerson(Person p);

    String[][] getAccounts();

    void editPerson(Person p,String name);

    void editAccount(int balance, int numberToEdit, String cnp);

    void deleteAccount(int numberToDelete, String owner);

    boolean isCNPvalid(String cnp);
    boolean accNrExists(int nr);
    boolean isPersonNameValid(String name);
    boolean isAccNrValid(int nr);
    boolean isBalValid(int bal);
    boolean isOnePlus(int nr);
    boolean ammountNeg(int nr);
    boolean isWellFormed();
    void read(Object o);
    void write(Object e);

}
