package model;

public class SpendingAccount extends Account {

    public SpendingAccount(int number, int balance, String owner) {
        super(number, balance, owner,"Spending");
    }
}
