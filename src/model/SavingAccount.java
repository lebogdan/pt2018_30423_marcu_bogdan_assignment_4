package model;

public class SavingAccount extends Account {
    private int dobanda;

    public SavingAccount(int number, int balance, String owner) {
        super(number, balance, owner,"Saving");
        this.dobanda = 10;
    }


    @Override
    public void addMoney(int ammount) {
        if(this.getBalance()==0)
        {
            this.setBalance(ammount);
        }
    }

    @Override
    public void withdrawMoney(int ammount) {
        ammount = this.getBalance();
        super.withdrawMoney(ammount);
    }

    @Override
    public String toString() {
        return "Account{" +
                "number=" + this.getNumber() +
                ", balance=" + this.getBalance() +
                ", dobanda=" + dobanda +
                ", owner='" + this.getOwner()+ '\'' +
                '}';
    }
}
