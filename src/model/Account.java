package model;

import java.io.Serializable;
import java.util.Observable;

public abstract class Account implements Serializable {

    private int number;
    private int balance;
    private String owner;
    private String type;



    public Account(int number, int balance, String owner,String type) {
        this.number = number;
        this.balance = balance;
        this.owner = owner;
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void addMoney(int ammount){
        this.balance+=ammount;
    }

    public void withdrawMoney(int ammount){
        this.balance-=ammount;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Account{" +
                "number=" + number +
                ", balance=" + balance +
                ", owner='" + owner + '\'' +
                '}';
    }
}
