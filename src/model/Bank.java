package model;

import java.io.*;
import java.util.*;

public class Bank implements BankProc, Serializable {

        public HashMap<Person,HashSet<Account>> persons;// = new HashMap<Person,List<Account>>();

    public Bank(){
        persons = new HashMap<Person,HashSet<Account>>();
        read(persons);
    }


    public void addPerson(Person person){
        assert  isWellFormed() : "not well formed";
        assert(isPersonNameValid(person.getName())) : "name invalid";
        assert CNPexists(person.getCnp())  : "cnp exists";
        assert isCNPvalid(person.getCnp()) : "cnp invalid";
        int nr = persons.size();
        HashSet<Account> empty = new HashSet<>();
        persons.put(person,empty);
        assert isOnePlus(nr): "n-ai bagat persoana baiatu' meu";
        write(persons);
    }




    public void addSavingAccount(String owner, SavingAccount account) {
        assert  isWellFormed() : "not well formed";
        HashSet<Account> aux = new HashSet<>();

        int nr=0;
        assert isAccNrValid(account.getNumber()):"acc nr invalid";
        assert isBalValid(account.getBalance()):"acc bal invalid";
        assert accNrExists(account.getNumber()):"acc number already exists";

        for(Person p : persons.keySet()){
            if(p.getCnp().equals(owner)){
                nr = persons.get(p).size();
                aux = persons.get(p);
                aux.add(account);
                persons.replace(p,aux);
                assert isOnePlusAcc(nr,owner): "n-ai bagat cont";
                write(persons);
                return;
            }
        }
    }

    public void addSpendingAccount(String owner, SpendingAccount account) {
        assert  isWellFormed() : "not well formed";
        HashSet<Account> aux = new HashSet<>();
        int nr;
        assert isAccNrValid(account.getNumber()):"acc nr invalid";
        assert isBalValid(account.getBalance()):"acc bal invalid";
        assert accNrExists(account.getNumber()):"acc number already exists";

        for(Person p : persons.keySet()){
            if(p.getCnp().equals(owner)){
                nr = persons.get(p).size();
                aux = persons.get(p);
                aux.add(account);
                persons.replace(p,aux);
                assert isOnePlusAcc(nr,owner): "n-ai bagat cont";
                write(persons);
                return;
            }
        }


    }

    public void addSpendingMoney(String owner, int number, int ammmount){

        HashSet<Account> aux = new HashSet<>();
        assert  isWellFormed() : "not well formed";
        assert isCNPvalid(owner) : "cnp invalid";
        assert !CNPexists(owner) : "cnp doesn't exist";
        assert ammountNeg(ammmount): "ammount invalid";
        for(Person p : persons.keySet()){
            if(p.getCnp().equals(owner)){
                for(Account a: persons.get(p)){
                    if (a.getNumber()==number){
                        if (a.getType() == "Saving") {
                            if(a.getBalance() == 0){
                                aux= persons.get(p);
                                aux.remove(a);
                                a.addMoney(ammmount);
                                aux.add(a);
                                persons.replace(p,aux);
                                System.out.println(aux);
                                write(persons);
                                return;
                            }
                            else return;
                        }

                        else{
                            aux= persons.get(p);
                            aux.remove(a);
                            a.addMoney(ammmount);
                            aux.add(a);
                            persons.replace(p,aux);
                            System.out.println(aux);
                            write(persons);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void increaseDobanda(){
        assert  isWellFormed() : "not well formed";
        for(Person p : persons.keySet()){
            for(Account a: persons.get(p))
            {
                if(a.getType().equals("Saving")){
                    a.setBalance(a.getBalance()+(10*(a.getBalance())/100));
                    write(persons);
                }
            }
        }
    }

    public void withdrawSpendingMoney(String owner, int number, int ammmount){
        assert isCNPvalid(owner) : "cnp invalid";
        assert !CNPexists(owner) : "cnp doesn't exist";
        assert ammountNeg(ammmount): "ammount invalid";
        assert  isWellFormed() : "not well formed";

        HashSet<Account> aux = new HashSet<>();
        for(Person p : persons.keySet()){
            if(p.getCnp().equals(owner)){
                for(Account a: persons.get(p)){
                    if (a.getNumber()==number){
                        if (a.getType() == "Saving") {
                            if(a.getBalance() > 0){
                                aux= persons.get(p);
                                aux.remove(a);
                                a.setBalance(0);
                                aux.add(a);
                                persons.replace(p,aux);
                                write(persons);
                                return;
                            }
                            else return;
                        }

                        else{
                            aux= persons.get(p);
                            aux.remove(a);
                            a.withdrawMoney(ammmount);
                            aux.add(a);
                            persons.replace(p,aux);
                            System.out.println(aux);
                            write(persons);
                            return;
                        }
                    }
                }
            }
        }

    }

    public void deletePerson(Person p){
        assert  isWellFormed() : "not well formed";
        int nr = persons.size();
        for(Person pers : persons.keySet()){
        if(pers.getCnp().equals(p.getCnp()))
            persons.remove(pers);
            write(persons);
            return;
        }
        assert !isOnePlus(nr): "n-ai sters persoana, amice";
    }

    public String[][] getPersons(){

        int i = 0;
        String data[][] = new String[50][50];
        for(Person p : persons.keySet()){
            data[i][0] = p.getName();
            data[i][1] = p.getCnp();
            i++;
        }
        return data;
    }

    public String[][] getAccounts(){
        int i = 0;
        //int j = 0;
        String[][] data = new String[50][50];
        Set<Person> pers = this.persons.keySet();
        for(Person p : pers){
            HashSet<Account> accs =  this.persons.get(p);

            for(Account a : accs){
                String nr = Integer.toString(a.getNumber());
                String bal = Integer.toString(a.getBalance());
                String owner = a.getOwner();
                String type = a.getType();

                data[i][0] = nr;
                //System.out.println(data[i][0]);
                //j++;
                data[i][1] = bal;
                //System.out.println(data[i][1]);
                //j++;
                data[i][2] = owner;
                //System.out.println(data[i][2]);
                data[i][3] = type;
                i++;
            }

            //j = 0;
        }


        /*for(int k =0; k< 3; k++){
            for(int l = 0; l< 3; l++)
                System.out.println(data[k][l]);
        }*/

        return data;
    }

    public void editPerson(Person p,String name){
        assert  isWellFormed() : "not well formed";
        assert isPersonNameValid(name) : "invalid name";
        HashSet<Account> aux = new HashSet<>();

        for(Person pers : persons.keySet()){
            if(pers.getCnp().equals(p.getCnp()))
                aux = persons.get(p);
                persons.remove(pers);
                p.setName(name);
                persons.put(p,aux);
            write(persons);
                return;
        }
    }



    public void editAccount(int balance, int numberToEdit, String cnp){
        HashSet<Account> aux = new HashSet<>();
        assert  isWellFormed() : "not well formed";
        assert !CNPexists(cnp) : "cnp inexistent";
        assert isBalValid(balance) : "balance invalid";
        assert isAccNrValid(numberToEdit) : "acc nr invalid";

        for(Person p : persons.keySet()){
            if (p.getCnp().equals(cnp)){
                aux = persons.get(p);
                //System.out.println(aux);
                //System.out.println(numberToEdit);

                    for(Account a : aux){

                        if(a.getNumber() == numberToEdit){

                            aux.remove(a);
                            a.setBalance(balance);
                            aux.add(a);
                            persons.replace(p,aux);
                            write(persons);
                            return;
                        }

                    }

                return;
            }
        }
    }

    public void deleteAccount(int numberToDelete, String owner){
        assert  isWellFormed() : "not well formed";
        assert !CNPexists(owner) : "no such owner";
        assert isAccNrValid(numberToDelete) : "invalid number";

        HashSet<Account> aux = new HashSet<>();
        int nr = 0;
        for(Person p : persons.keySet()){
            if(p.getCnp().equals(owner)){
                for(Account a : persons.get(p)){
                    if(a.getNumber() == numberToDelete){
                        nr = persons.get(p).size();
                        aux = persons.get(p);
                        aux.remove(a);
                        persons.replace(p,aux);
                        assert isOnePlus(nr): "n-ai scos cont";
                        write(persons);
                        return;
                    }
                }
            }
        }
    }

    public void write(Object e){
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("bank.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(persons);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in /tmp/bank.ser");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void read(Object o){
        try {
            FileInputStream fileIn = new FileInputStream("bank.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            persons = (HashMap<Person, HashSet<Account>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("HashMap class not found");
            c.printStackTrace();
            return;
        }

    }


    //pre-post conditions:

     public boolean CNPexists(String cnp){
        for (Person p : persons.keySet()){
            if(p.getCnp().equals(cnp)) return false;
         }

         return true;
     }

     public boolean accNrExists(int nr){
         for(Person p : persons.keySet()){
             for(Account a : persons.get(p)){
                 if(a.getNumber()==nr) return false;
             }
         }
         return true;
     }

     public boolean isCNPvalid(String cnp){
        if(cnp.length() == 10) return true;
        else return false;

    }

    public boolean isOnePlus(int nr){
         if(persons.size()>nr) return true;
         else return false;
    }

    public boolean isOnePlusAcc(int nr, String cnp){
        for(Person p : persons.keySet()){
            if(p.getCnp().equals(cnp)){
                if(persons.get(p).size()>nr) return true;
            }
        }
        return false;
    }

     public boolean isPersonNameValid(String name){
        if (name.length()>=4) return true;
        else return false;
     }

     public boolean isAccNrValid(int nr){
            if(nr >= 10000 && nr <=  99999) return true;
            else return false;
     }

     public boolean ammountNeg(int nr){
         if(nr>0) return true;
         else return false;
     }

     public boolean isBalValid(int bal){
         if(bal >= 0) return true;
         else return false;

     }



     public boolean isWellFormed(){
         for(Person p : persons.keySet()){
             if (persons.get(p) == null) return false;
         }
         return true;
     }


}
