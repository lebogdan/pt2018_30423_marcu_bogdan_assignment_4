package view;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class UI extends JFrame {
    private static final long serialVersionUID = 1L;
    private int WIDTH = 700;
    private int HEIGHT = 700;
    private JPanel panel;
    private String[] persData = {"name","cnp"};
    private String[] accData = {"number","balance", "owner","type"};

    private Bank bank = new Bank();



    public UI(){
        panel = new JPanel();
        this.add(panel);
        panel.setLayout(null);
        this.setSize(WIDTH, HEIGHT);
        /*
        Person pers = new Person("gigel", "1234567890");
        Person pers2 = new Person("petrescu","1234567891");
        SavingAccount test1 = new SavingAccount(12396,0,"1234567890");
        SpendingAccount test4 = new SpendingAccount(12333,350,"1234567890");
        SpendingAccount test2 = new SpendingAccount(12345, 3000,"1234567890");
        SpendingAccount test3 = new SpendingAccount(12399,550,"1234567891");
        bank.addPerson(pers);
        bank.addPerson(pers2);
        bank.addSavingAccount("1234567890",test1);
        bank.addSpendingAccount("1234567890",test4);
        bank.addSpendingAccount("1234567890",test2);
        bank.addSpendingAccount("1234567891",test3);
        */
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void beginWindow(){
        panel.removeAll();
        panel.repaint();
        panel.revalidate();
        panel.setLayout(null);

        JButton buttonPerson = new JButton("Person");
        JButton buttonAccount = new JButton("Account");


        buttonPerson.setBounds(150,350,100,40);
        buttonAccount.setBounds(450,350,100,40);


        panel.add(buttonPerson);
        panel.add(buttonAccount);


        buttonPerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                personWindow();
            }
        });

        buttonAccount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                accountWindow();
            }
        });


    }

    public void personWindow(){
        panel.removeAll();
        panel.setLayout(null);



        JButton add = new JButton("add");
        JButton edit = new JButton("edit");
        JButton delete = new JButton("delete");
        JButton viewAll = new JButton("view all");
        JButton back = new JButton("back");
        back.setBounds(50,600,100,50);
        panel.add(back);

        JLabel name = new JLabel("name");
        JLabel cnp = new JLabel("CNP");
        JLabel nname = new JLabel("new name");
        JLabel ncnp = new JLabel("new CNP");


        name.setBounds(115,90,50,10);
        cnp.setBounds(115,190,50,10);
        nname.setBounds(115,290,60,10);
        ncnp.setBounds(115,390,60,10);


        JTextField nameText = new JTextField();
        JTextField cnpText = new JTextField();
        JTextField newName = new JTextField();
        JTextField newCNP = new JTextField();


        nameText.setBounds(115,100,100,40);
        nameText.setText("NAME");
        cnpText.setBounds(115,200,100,40);
        cnpText.setText("CNP");
        newName.setBounds(115,300,100,40);
        newCNP.setBounds(115,400,100,40);


        add.setBounds(10,20,100,40);
        edit.setBounds(115,20,100,40);
        delete.setBounds(235,20,100,40);
        viewAll.setBounds(350,20,100,40);

        panel.add(nameText);
        panel.add(name);
        //panel.add(ncnp);
        panel.add(nname);
        panel.add(newName);
        //panel.add(newCNP);
        panel.add(cnp);
        panel.add(nameText);
        panel.add(cnpText);

        panel.add(add);
        panel.add(edit);
        panel.add(delete);
        panel.add(viewAll);

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Person aux = new Person(nameText.getText(),cnpText.getText());
                bank.addPerson(aux);

            }
        });

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bank.editPerson((new Person(nameText.getText(),cnpText.getText())),newName.getText());

            }
        });

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bank.deletePerson((new Person(nameText.getText(),cnpText.getText())));
            }
        });

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.removeAll();
                beginWindow();
            }
        });

        viewAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTable jTable1 = new JTable(bank.getPersons(),persData);
                JScrollPane scrollPane1 = new JScrollPane(jTable1);
                scrollPane1.setBounds(250,150,400,300);
                panel.add(scrollPane1);

                jTable1.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        cnpText.setText(jTable1.getModel().getValueAt(jTable1.getSelectedRow(),1).toString());
                        nameText.setText(jTable1.getModel().getValueAt(jTable1.getSelectedRow(),0).toString());

                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }

                });}
        });
        panel.revalidate();
        panel.repaint();

    }

    public void accountWindow(){
        panel.removeAll();
        panel.setLayout(null);

        JButton add = new JButton("add acc");
        JButton addMoney = new JButton("add money");
        JButton edit = new JButton("edit");
        JButton delete = new JButton("delete");
        JButton viewAll = new JButton("view all");
        JButton takeMoney = new JButton("take money");
        JCheckBox saving = new JCheckBox("saving");
        JButton back = new JButton("back");
        JButton passTime=  new JButton("pass time");
        back.setBounds(50,600,100,50);
        panel.add(back);

        JLabel accountNr = new JLabel("number");
        JLabel balance = new JLabel("balance");
        JLabel dobanda = new JLabel("dobanda");
        JLabel ammount = new JLabel("ammount");
        JLabel cnp = new JLabel("cnp");


        accountNr.setBounds(115,90,50,10);
        balance.setBounds(115,190,50,10);
        dobanda.setBounds(115, 290,50,10);
        ammount.setBounds(115,490,90,10);
        cnp.setBounds(114,390,50,10);


        JTextField accountNrText = new JTextField("Number");
        JTextField balanceText = new JTextField("Balance");
        //JTextField dobandaText = new JTextField("Dobanda");
        JTextField ammountText = new JTextField("Ammount");
        JTextField cnpText = new JTextField("CNP");


        accountNrText.setBounds(115,100,100,40);
        balanceText.setBounds(115,200,100,40);
        //dobandaText.setBounds(115, 300, 100, 40);
        ammountText.setBounds(115,500,100,40);
        cnpText.setBounds(115,400,100,40);



        add.setBounds(10,20,100,40);
        edit.setBounds(115,20,100,40);
        delete.setBounds(235,20,100,40);
        viewAll.setBounds(350,20,100,40);
        saving.setBounds(465,20,100,40);
        addMoney.setBounds(10,150,100,100);
        takeMoney.setBounds(10,300,100,100);
        passTime.setBounds(10,450,100,100);


        panel.add(ammount);
        panel.add(passTime);
        panel.add(takeMoney);
        panel.add(ammountText);
        panel.add(addMoney);
        panel.add(accountNrText);
        panel.add(accountNr);
        panel.add(balance);
        panel.add(accountNrText);
        panel.add(balanceText);
        panel.add(cnp);
        panel.add(cnpText);

        panel.add(add);
        panel.add(edit);
        panel.add(delete);
        panel.add(viewAll);
        panel.add(saving);

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(saving.isSelected()){
                    SavingAccount aux = new SavingAccount((Integer.parseInt(accountNrText.getText())),Integer.parseInt(balanceText.getText()),cnpText.getText());
                    bank.addSavingAccount(cnpText.getText(),aux);
                }
                else
                {
                    SpendingAccount aux = new SpendingAccount((Integer.parseInt(accountNrText.getText())),Integer.parseInt(balanceText.getText()),cnpText.getText());
                    bank.addSpendingAccount(cnpText.getText(),aux);
                }

            }
        });

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bank.editAccount(Integer.parseInt(balanceText.getText()),Integer.parseInt(accountNrText.getText()),cnpText.getText());
            }
        });

        addMoney.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                    bank.addSpendingMoney(cnpText.getText(),Integer.parseInt(accountNrText.getText()),Integer.parseInt(ammountText.getText()));

            }
        });

        takeMoney.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                bank.withdrawSpendingMoney(cnpText.getText(),Integer.parseInt(accountNrText.getText()),Integer.parseInt(ammountText.getText()));

            }
        });



        passTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bank.increaseDobanda();
            }
        });

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bank.deleteAccount(Integer.parseInt(accountNrText.getText()),cnpText.getText());

            }
        });

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.removeAll();
                beginWindow();
            }
        });


        viewAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JTable jTable = new JTable(bank.getAccounts(),accData);
                JScrollPane scrollPane = new JScrollPane(jTable);
                scrollPane.setBounds(250,150,400,300);
                panel.add(scrollPane);

                jTable.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        cnpText.setText(jTable.getModel().getValueAt(jTable.getSelectedRow(),2).toString());
                        balanceText.setText(jTable.getModel().getValueAt(jTable.getSelectedRow(),1).toString());
                        accountNrText.setText(jTable.getModel().getValueAt(jTable.getSelectedRow(),0).toString());
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }


                });

            }
        });
        panel.revalidate();
        panel.repaint();

    }


}
